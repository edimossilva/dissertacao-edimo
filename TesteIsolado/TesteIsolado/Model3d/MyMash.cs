﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoKinect.Model3d
{
    class MyMash
    {
        private int vertexIndex1, vertexIndex2, vertexIndex3;
        private MyPoint3D normal;

        public MyMash(int vertexIndex1, int vertexIndex2, int vertexIndex3,MyPoint3D normal) {
            this.vertexIndex1 = vertexIndex1;
            this.vertexIndex2 = vertexIndex2;
            this.vertexIndex3 = vertexIndex3;
            this.normal = normal;
        }

        public MyPoint3D getNormal() {
            return normal;
        }
        public int getVertexIndex1()
        {
            return vertexIndex1;
        }
        public int getVertexIndex2()
        {
            return vertexIndex2;
        }
        public int getVertexIndex3()
        {
            return vertexIndex3;
        }
        public override string ToString()
        {
            return "V1= " + vertexIndex1 + " V2= " + vertexIndex2 + " V3= " + vertexIndex3;
        }
    }
}
