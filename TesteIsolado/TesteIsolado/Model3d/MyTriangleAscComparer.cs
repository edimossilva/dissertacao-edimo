﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoKinect.Model3d
{
    class MyTriangleAscComparer : IComparer<MyTriangle>
    {
        public int Compare(MyTriangle t1, MyTriangle t2)
        {
            if (t1.getLessZ() > t2.getLessZ())
            {
                return 1;
            }
            else if (t1.getLessZ() < t2.getLessZ())
            {
                return -1;
            }
            else {
                return 0;
            }
        }
    }
}
