﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace ProjetoKinect.Model3d
{
    public class MyTriangle
    {
        private MyPoint3D p0;
        private MyPoint3D p1;
        private MyPoint3D p2;
        private MyPoint3D pMedio;
        private Vector3D normal;
        private GeometryModel3D triangleModel;

        public MyTriangle(MyPoint3D p0, MyPoint3D p1, MyPoint3D p2, MyPoint3D normal)
        {
            this.p0 = p0;
            this.p1 = p1;
            this.p2 = p2;
            // this.normal = normal.toVector3D();
            this.normal = normal.toVector3D();
        }
        public Vector3D getNormal()
        {
            return normal;
        }
        public void rotateZ(double angle)
        {
            p0.rotateClockWiseZ(angle);
            p1.rotateClockWiseZ(angle);
            p2.rotateClockWiseZ(angle);
        }
        public void rotateX(double angle)
        {
            p0.rotateClockWiseX(angle);
            p1.rotateClockWiseX(angle);
            p2.rotateClockWiseX(angle);
        }
        public void rotateY(double angle)
        {
            p0.rotateClockWiseY(angle);
            p1.rotateClockWiseY(angle);
            p2.rotateClockWiseY(angle);
        }

        public Vector3D calculateNormal()
        {
            if (normal == null)
            {
                MyPoint3D p0p1 = p1.subtraction(p0);
                MyPoint3D p1p2 = p2.subtraction(p1);

                Vector3D p0p1Point = new Vector3D(p0p1.getX(), p0p1.getY(), p0p1.getZ());
                Vector3D p1p2Point = new Vector3D(p1p2.getX(), p1p2.getY(), p1p2.getZ());
                return Vector3D.CrossProduct(p0p1Point, p1p2Point);

            }
            return normal;
        }

        public double getAngleBetween(Vector3D vector1, Vector3D vector2)
        {

            return Math.Round(Vector3D.AngleBetween(vector1, vector2), 2);
        }
        public double getAngleBetween(MyPoint3D myPoint1, MyPoint3D myPoint2)
        {
            return getAngleBetween(myPoint1.toVector3D(), myPoint2.toVector3D());
        }

        public MyPoint3D getP0()
        {
            return p0;
        }
        public MyPoint3D getP1()
        {
            return p1;
        }
        public MyPoint3D getP2()
        {
            return p2;
        }
        public MyPoint3D getPMedio()
        {
            if (pMedio == null)
            {
                double x = (p0.getX() + p1.getX() + p2.getX()) / 3;
                double y = (p0.getY() + p1.getY() + p2.getY()) / 3;
                double z = (p0.getZ() + p1.getZ() + p2.getZ()) / 3;

                pMedio = new MyPoint3D(x, y, z);
            }
            return pMedio;
        }
        public void setTriangleModel(GeometryModel3D triangleModel)
        {
            this.triangleModel = triangleModel;
        }
        public GeometryModel3D getTriangleModel()
        {
            return this.triangleModel;
        }

        internal float getCos(double angle)
        {
            if (angle.Equals(double.NaN))
            {
                angle = 0;
            }
            double cosDouble = Math.Cos(angle * (Math.PI / 180.0));
            if (cosDouble < 0)
            {
                cosDouble = cosDouble * -1;
            }
            return (float)cosDouble;
        }

        public static short[] vector3DToShortArray(Vector3D vector3D)
        {
            short[] normalFloat = new short[3];
            normalFloat[0] = (short)vector3D.X;
            normalFloat[1] = (short)vector3D.Y;
            normalFloat[2] = (short)vector3D.Z;
            return normalFloat;
        }

        internal double getLessZ()
        {
            double lessZ = getP0().getZ();
            if (lessZ > getP1().getZ())
            {
                lessZ = getP1().getZ();
            }
            if (lessZ > getP2().getZ())
            {
                lessZ = getP2().getZ();
            }
            return lessZ;
        }



    }
}
