﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoKinect.Model3d
{
    class Util
    {
        public static double toSin(double angle)
        {
            double radians = (Math.PI / 180) * angle;
            radians = Math.Round(radians, 2);
            double sin = Math.Sin(radians);
            sin = Math.Round(sin, 2);

            return sin;
        }
        public static double toCos(double angle)
        {
            double radians = (Math.PI / 180) * angle;
            radians = Math.Round(radians, 2);
            double cos = Math.Cos(radians);
            cos = Math.Round(cos, 2);

            return cos;
        }


    }
}
