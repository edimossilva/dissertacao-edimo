﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ProjetoKinect.Model3d
{
    public class TriangleObject
    {
        private List<MyTriangle> triangleList = new List<MyTriangle>();
        private Color color = Colors.Blue;
        private double transparency=1;

        public TriangleObject()
        {

        }

        public TriangleObject(List<MyTriangle> triangleList, Color color, double opacity){
            this.triangleList = triangleList;
            this.color = color;
            this.transparency = opacity;
        }

        public List<MyTriangle> getTriangleList() {
            return triangleList;
        }

        public Color getColor()
        {
            return color;
        }
        public void setColor(Color color)
        {
            this.color = color;
        }
        public double getTransparency()
        {
            return transparency;
        }
        public void setTransparency(double transparency)
        {
            this.transparency = transparency;
        }

        internal void addTriangle(MyTriangle myTriangle)
        {
            triangleList.Add(myTriangle);
        }



       
    }
}
