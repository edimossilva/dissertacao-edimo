﻿using Leap;
using ProjetoKinect.enums;
using ProjetoKinect.LeapMotionGestures;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ProjetoKinect.View2d
{
    class LeapMotionListennerV2d : Listener
    {
        public View2dConfig view2dConfig;
        public View2D view2D;
        public LeapMotionController leapMotionController = new LeapMotionController();
        public long currentTime = DateTime.Now.Ticks;
        public long lastTime = DateTime.Now.Ticks;
        public bool shouldExit = false;
        private static LeapMotionListennerV2d leapMotionListennerV2d;

        public static LeapMotionListennerV2d getInstance()
        {
            if (leapMotionListennerV2d == null)
            {
                leapMotionListennerV2d = new LeapMotionListennerV2d();
            }
            return leapMotionListennerV2d;
        }

        private LeapMotionListennerV2d()
        {

        }

        public void setView2dConfig(View2dConfig view2dConfig) {
            this.view2dConfig = view2dConfig;
        }
        public void setView2D(View2D view2D) {
            this.view2D = view2D;
        }

        public override void OnFrame(Controller ctrl)
        {
            currentTime = DateTime.Now.Ticks;
            if (currentTime - lastTime > 100)
            {
                Frame frame = ctrl.Frame();
                lastTime = currentTime;
                leapMotionController.update(frame);
                if (leapMotionController.getHandsGesture().areTwoHandsOn())
                {
                    if (leapMotionController.getHandsGesture().areTwoHandsFingerCount(1))
                    {
                        enableTranslate();
                    }
                    else if (leapMotionController.getHandsGesture().areTwoHandsFingerCount(2))
                    {
                        enableZoom();
                    }
                    if (leapMotionController.getHandsGesture().hasAnyHandClosed())
                    {
                        if (leapMotionController.getHandsGesture().getFingersCount() == 2)
                        {
                            if (leapMotionController.getHandsGesture().isCircular())
                            {
                                shouldExit = true;
                                //Console.Beep();
                            }
                        }
                    }
                }
                else if (leapMotionController.getHandsGesture().isOneHandOn())
                {
                    if (view2dConfig.isTranslateEnabled())
                    {
                        translateWithGesture();
                    }
                    else if (view2dConfig.isZoomEnabled())
                    {
                        zoomWithGesture();
                    }
                }

            }

            manipuleView2d();

        }

        private void zoomWithGesture()
        {
            if (leapMotionController.getHandsGesture().isOneHandClosed())
            {
                if (leapMotionController.getHandZOscilationGesture().getMovmentStatus().Equals(MovimentStatus.INCREASING))
                {
                    view2dConfig.increazeZoom(5);
                }
                else if (leapMotionController.getHandZOscilationGesture().getMovmentStatus().Equals(MovimentStatus.DECREASING))
                {
                    view2dConfig.increazeZoom(-5);
                }

            }
        }

        private void translateWithGesture()
        {
            if (leapMotionController.getHandsGesture().isOneHandClosed())
            {
                if (leapMotionController.getHandXOscilationGesture().getMovmentStatus().Equals(MovimentStatus.INCREASING))
                {
                    view2dConfig.increasePlaceX(-5);
                }
                else if (leapMotionController.getHandXOscilationGesture().getMovmentStatus().Equals(MovimentStatus.DECREASING))
                {
                    view2dConfig.increasePlaceX(5);
                }
                if (leapMotionController.getHandYOscilationGesture().getMovmentStatus().Equals(MovimentStatus.INCREASING))
                {
                    view2dConfig.increasePlaceY(5);
                }
                else if (leapMotionController.getHandYOscilationGesture().getMovmentStatus().Equals(MovimentStatus.DECREASING))
                {
                    view2dConfig.increasePlaceY(-5);
                }
            }
        }

        private void enableZoom()
        {
            view2dConfig.setTranslateEnabled(false);
            view2dConfig.setZoomEnabled(true);
        }

        private void enableTranslate()
        {
            view2dConfig.setTranslateEnabled(true);
            view2dConfig.setZoomEnabled(false);
        }

        public void manipuleView2d()
        {
            view2D.leapMotionManipulation();
        }
    }
}
