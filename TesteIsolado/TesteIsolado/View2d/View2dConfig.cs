﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoKinect.View2d
{
    class View2dConfig
    {
        private int imageZoom = 500;
        private int placeX = 0;
        private int placeY = 0;
        private bool zoomEnabled = false;
        private bool translateEnabled = false;

        public View2dConfig( int placeX, int placeY,int imageZoom) {
            this.placeX = placeX;
            this.placeY = placeY;
            this.imageZoom = imageZoom;
        }

        public int getPlaceX() {
            return placeX;
        }
        
        public int getPlaceY()
        {
            return placeY;
        }
        
        public int getImageZoom()
        {
            return imageZoom;
        }

        internal void increasePlaceX(int p)
        {
            placeX += p;
        }
        
        internal void increasePlaceY(int p)
        {
            placeY += p;
        }

        internal void increazeZoom(int p)
        {
            imageZoom += p;
        }

        internal void setTranslateEnabled(bool p)
        {
            translateEnabled = p;    
        }

        internal void setZoomEnabled(bool p)
        {
            zoomEnabled = p;
        }

        internal bool isZoomEnabled()
        {
            return zoomEnabled;
        }

        internal bool isTranslateEnabled()
        {
            return translateEnabled;
        }
    }
}
