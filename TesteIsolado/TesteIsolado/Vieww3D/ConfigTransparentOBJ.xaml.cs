﻿using HiSharpGL;
using Leap;
using LeapMotionMouse;
using Microsoft.Kinect.Toolkit.Controls;
using ProjetoKinect.LeapMotionGestures;
using ProjetoKinect.Model;
using ProjetoKinect.Model3d;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetoKinect.Vieww3D
{

    public partial class ConfigTransparentOBJ : Window
    {
        private List<TriangleObject> triangleObjectList;
        private float scale = 1.0f;
        private float angleY = 0.0f;
        private float angleX = 0.0f;
        private KinectController kinectController;
        private Controller lmController = LeapMotionController.getInstance();
        private MouseListener mouseListener = MouseListener.getInstance();

        public ConfigTransparentOBJ(List<TriangleObject> triangleObjectList, float scale, float angleX, float angleY)
        {
            InitializeComponent();
            this.triangleObjectList = triangleObjectList;
            this.scale = scale;
            this.angleX = angleX;
            this.angleY = angleY;
            kinectController = new KinectController(this.kinectRegion);
            kinectController.setKinectSensor();
            lmController.AddListener(mouseListener);
            paintAllButtons();
        }
     
        private void paintAllButtons()
        {

            paintInterButton();
            paintBoneButton();
            paintExternButton();
                    
        }

        private void paintExternButton()
        {
            if (triangleObjectList.Count > 2)
            {
                if (isInvisible(triangleObjectList[2]))
                {
                    paintPressed(objExternItem1);
                }
                else
                {
                    paintBluee(objExternItem1);
                }
                if (isTransparent(triangleObjectList[2]))
                {
                    paintPressed(objExternItem2);
                }
                else
                {
                    paintBluee(objExternItem2);
                }
                if (isOpaque(triangleObjectList[2]))
                {
                    paintPressed(objExternItem3);
                }
                else
                {
                    paintBluee(objExternItem3);
                }
            }
            else {

                turnUntoachble(objExternItem1, objExternItem2, objExternItem3);
            }
        }

        private void paintBoneButton()
        {
            if (triangleObjectList.Count > 1)
            {
                if (isInvisible(triangleObjectList[1]))
                {
                    paintPressed(objBoneItem1);
                }
                else
                {
                    paintBluee(objBoneItem1);
                }
                if (isTransparent(triangleObjectList[1]))
                {
                    paintPressed(objBoneItem2);
                }
                else
                {
                    paintBluee(objBoneItem2);
                }
                if (isOpaque(triangleObjectList[1]))
                {
                    paintPressed(objBoneItem3);
                }
                else
                {
                    paintBluee(objBoneItem3);
                }
            }
            else {
                turnUntoachble(objBoneItem1, objBoneItem2, objBoneItem3);
            }
        }

        private void paintInterButton()
        {
            if (triangleObjectList[0] != null)
            {
                if (isInvisible(triangleObjectList[0]))
                {
                    paintPressed(objInternItem1);
                }
                else
                {
                    paintBluee(objInternItem1);
                }
                if (isTransparent(triangleObjectList[0]))
                {
                    paintPressed(objInternItem2);
                }
                else
                {
                    paintBluee(objInternItem2);
                }

                if (isOpaque(triangleObjectList[0]))
                {
                    paintPressed(objInternItem3);
                }
                else
                {
                    paintBluee(objInternItem3);
                }
            }
            else {
                turnUntoachble(objInternItem1, objInternItem2, objInternItem3);
            }
        }

        private void turnUntoachble(KinectTileButton kinectTileButton1, KinectTileButton kinectTileButton2, KinectTileButton kinectTileButton3)
        {
            kinectTileButton1.IsEnabled = false;
            kinectTileButton2.IsEnabled = false;
            kinectTileButton3.IsEnabled = false;
        }

        private void paintAllButtonsPressed()
        {
            objInternItem1.IsEnabled = false;
            objInternItem2.IsEnabled = false;
            objInternItem3.IsEnabled = false;

            objBoneItem1.IsEnabled = false;
            objBoneItem2.IsEnabled = false;
            objBoneItem3.IsEnabled = false;

            objExternItem1.IsEnabled = false;
            objExternItem2.IsEnabled = false;
            objExternItem3.IsEnabled = false;
        }

        private bool isInvisible(TriangleObject triangleObject)
        {
            if (triangleObject.getTransparency() == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool isTransparent(TriangleObject triangleObject)
        {
            if (triangleObject.getTransparency() > 0 && triangleObject.getTransparency()<1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool isOpaque(TriangleObject triangleObject)
        {
            if (triangleObject.getTransparency() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void exitButtonClick(object sender, RoutedEventArgs e)
        {
            this.kinectController.stopFullSensor();
            this.lmController.RemoveListener(mouseListener);
            new OpenGLWindow(triangleObjectList, scale, angleX, angleY).Show();
            
            this.Close();
        }

        private void paintPressed(KinectTileButton obj0item1)
        {
            BitmapImage bi = new BitmapImage(new Uri(@"C:\Imagens\Default\blueButton2.png"));
            obj0item1.Background = new ImageBrush(bi);
        }

        private void paintWeakBlue(KinectTileButton obj0item1)
        {

            Color blue1 = Colors.Blue;
            blue1.R = 200;
            blue1.G = 200;
            blue1.B = 255;

            obj0item1.Background = new SolidColorBrush(blue1);
        }

        private void paintBluee(KinectTileButton obj0item1)
        {
            BitmapImage bi = new BitmapImage(new Uri(@"C:\Imagens\Default\blueButton1.png"));
            obj0item1.Background = new ImageBrush(bi);
        }

        private void paintBlueGradient(KinectTileButton obj0item1)
        {
            Color blue1 = Colors.Blue;
            Color blue2 = Colors.Blue;
            LinearGradientBrush gradientBrush = new LinearGradientBrush();
            gradientBrush.StartPoint = new Point(0.5, 0);
            gradientBrush.EndPoint = new Point(0.5, 1);
            GradientStop gs1 = new GradientStop();
            blue1 = Colors.Blue;
            blue1.R = 150;
            blue1.G = 250;
            blue1.B = 255;
            gs1.Color = blue1;
            gs1.Offset = 0;
            GradientStop gs2 = new GradientStop();
            blue2 = Colors.Blue;
            blue2.R = 30;
            blue2.G = 235;
            blue2.B = 255;
            gs2.Color = blue2;
            gs2.Offset = 0.425;
            gradientBrush.GradientStops.Add(gs1);
            gradientBrush.GradientStops.Add(gs2);

            obj0item1.Background = gradientBrush;
        }
        /*
        private void paintGray(KinectCircleButton obj0item1)
        {
            Color blue = Colors.Blue;
            blue.R = 240;
            blue.G = 240;
            blue.B = 240;

            obj0item1.Background = new SolidColorBrush(blue);
        }*/
        private void turnObj0Invisible(object sender, RoutedEventArgs e)
        {
            triangleObjectList[0].setTransparency(0.0);
            
            paintPressed(objInternItem1);
            paintBluee(objInternItem2);
            paintBluee(objInternItem3);
        }


        private void turnObj0Transparent(object sender, RoutedEventArgs e)
        {
            triangleObjectList[0].setTransparency(0.2);

            paintPressed(objInternItem2);

            paintBluee(objInternItem1);
            paintBluee(objInternItem3);

        }
        private void turnObj0Opaque(object sender, RoutedEventArgs e)
        {
            triangleObjectList[0].setTransparency(1);

            paintPressed(objInternItem3);

            paintBluee(objInternItem1);
            paintBluee(objInternItem2);
        }

        private void turnObj1Invisible(object sender, RoutedEventArgs e)
        {
            triangleObjectList[1].setTransparency(0.0);

            paintPressed(objBoneItem1);

            paintBluee(objBoneItem2);
            paintBluee(objBoneItem3);
        }

        private void turnObj1Transparent(object sender, RoutedEventArgs e)
        {
            triangleObjectList[1].setTransparency(0.2);

            paintPressed(objBoneItem2);

            paintBluee(objBoneItem1);
            paintBluee(objBoneItem3);
        }
        private void turnObj1Opaque(object sender, RoutedEventArgs e)
        {
            triangleObjectList[1].setTransparency(1);

            paintPressed(objBoneItem3);

            paintBluee(objBoneItem1);
            paintBluee(objBoneItem2);
        }

        private void turnObj2Invisible(object sender, RoutedEventArgs e)
        {
            triangleObjectList[2].setTransparency(0.0);

            paintPressed(objExternItem1);

            paintBluee(objExternItem2);
            paintBluee(objExternItem3);
        }
        private void turnObj2Transparent(object sender, RoutedEventArgs e)
        {
            triangleObjectList[2].setTransparency(0.2);

            paintPressed(objExternItem2);

            paintBluee(objExternItem1);
            paintBluee(objExternItem3);
        }
        private void turnObj2Opaque(object sender, RoutedEventArgs e)
        {
            triangleObjectList[2].setTransparency(1);

            paintPressed(objExternItem3);

            paintBluee(objExternItem1);
            paintBluee(objExternItem2);
        }

        public List<TriangleObject> getTriangleObjectList()
        {
            return triangleObjectList;
        }

        public float getScale()
        {
            return scale;
        }

        public float getAngleX()
        {
            return angleX;
        }

        public float getAngleY()
        {
            return angleY;
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.kinectController.stopSensor();
        }
    }
}
