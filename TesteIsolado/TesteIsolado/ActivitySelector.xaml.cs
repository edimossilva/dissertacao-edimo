﻿using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ProjetoKinect.Model;
using HiSharpGL;
using Leap;
using LeapMotionMouse;
using ProjetoKinect.LeapMotionGestures;
using ProjetoKinect.Test;

namespace ProjetoKinect
{
    /// <summary>
    /// Interaction logic for ActivitySelector.xaml
    /// </summary>
    public partial class ActivitySelector : Window
    {
        public KinectController kinectController;
        private Controller lmController = LeapMotionController.getInstance();
        private MouseListener mouseListener = MouseListener.getInstance();

        public ActivitySelector(bool hasMouseListenner)
        {
            InitializeComponent();
            initializeGestureDevices(hasMouseListenner);
            setButtonImages();
           
        }
        public ActivitySelector()
        {
            InitializeComponent();
            initializeGestureDevices(false);
            setButtonImages();
          //  new TestWindow().Show();
          //  this.Close();
        }
        /*
        public ActivitySelector(Boolean useView3D,bool hasMouseListenner)
        {
            InitializeComponent();
            initializeGestureDevices(false);
            setButtonImages();
            if (useView3D) {
                 kinectController.stopSensor();
                 this.gridRegion.Children.Remove(kinectRegion);
                 this.gridRegion.Children.Add(new View3D(this));
            }
        }
       */
        public void initializeGestureDevices(bool hasMouseListener)
        {
            kinectController = new KinectController(this.kinectRegion);
            kinectController.setKinectSensor();
            if (!hasMouseListener)
            {
                lmController.AddListener(mouseListener);
            }
        }
        
        public void startSensor() {
            kinectController.startSensor();
        }
        public void stopSensor()
        {
            kinectController.stopSensor();
        }
        public void stopFullSensor()
        {
            kinectController.stopFullSensor();
        }
        public void setButtonImages(){
            BitmapImage biButton2D = new BitmapImage(new Uri(@"C:\Imagens\modelo\modelo2D4.png"));
            image2DButton.Background = new ImageBrush(biButton2D);
            BitmapImage biButton3D = new BitmapImage(new Uri(@"C:\Imagens\modelo\modelo3d2.jpg"));
            image3DButton.Background = new ImageBrush(biButton3D);
        }
       
        
        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // controller.RemoveListener(mouseListener);
           // controller.Dispose();
           // lmController.Dispose();
        }

        private void KinectTileButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void image2DButtonClick(object sender, RoutedEventArgs e)
        {
            this.kinectController.stopFullSensor();
            new Select2DWindow().Show();
            this.Close();
        }

        private void image3DButtonClick(object sender, RoutedEventArgs e)
        {
            kinectController.stopFullSensor();
           // new ActivitySelector(true).Show();
            new OpenGLWindow().Show();
            lmController.RemoveListener(mouseListener);
            this.Close();

        }

        
    }
}
