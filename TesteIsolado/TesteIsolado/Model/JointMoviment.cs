﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ProjetoKinect.enums;

namespace ProjetoKinect.Model
{
   
    class JointMoviment
    {
        private ArrayList recentJointPositions = new ArrayList();
        private float absolutValue;

        public void setAbsolutValue(float absolutValue) {
            this.absolutValue = absolutValue;
        }

        public float getAbsolutValue() {
            return absolutValue;
        }

        public void addJointPosition(float bodyJointAxisPosition, Axis axis)
        {
            setAbsolutValue(bodyJointAxisPosition);
            bodyJointAxisPosition = (float)Math.Round(bodyJointAxisPosition, 1);
        
            if (recentJointPositions.Count == 0)
            {
                recentJointPositions.Add(new JointAxis(bodyJointAxisPosition,axis));
            }
            else {
                JointAxis lastHandPosition = getLastElemnt(recentJointPositions);
                if (lastElementEquals(recentJointPositions,bodyJointAxisPosition))
                {
                    lastHandPosition.incrementQuantity();      
                }
                else
                {
                    recentJointPositions.Add(new JointAxis(bodyJointAxisPosition,axis));
                }
                if (recentJointPositions.Count == 11)
                {
                    recentJointPositions.Remove(0);
                }
                
            }
        
        }
        
        private bool lastElementEquals(ArrayList bodyJointPositionArray, float position)
        {
            JointAxis lastHandPosition = getLastElemnt(bodyJointPositionArray);
            if (lastHandPosition.getAxisPosition() == position || lastHandPosition.getAxisPosition() == position + 0.1 || lastHandPosition.getAxisPosition() == position-0.1)
            {
                return true;
            }
            return false;
        }
       
        public JointAxis getLastElemnt(ArrayList bodyJointPosituonArray)
        {
            return (JointAxis)bodyJointPosituonArray[bodyJointPosituonArray.Count - 1];
        }
        public JointAxis getBeforeLastElemnt(ArrayList handArray)
        {
            return (JointAxis)handArray[handArray.Count - 2];
        }
        public JointAxis getLastElemnt()
        {
            return getLastElemnt(recentJointPositions);
        }
        public float getLastElementPosition() {
            return getLastElemnt(recentJointPositions).getAxisPosition();
        }
        public MovimentStatus getMovimentStatus()
        {
            if (recentJointPositions.Count > 1)
            {
                if (getLastElemnt(recentJointPositions).getAxisPosition() > getBeforeLastElemnt(recentJointPositions).getAxisPosition())
                {
                    if (getLastElemnt(recentJointPositions).isAxisMoving())
                    {
                        return MovimentStatus.DECREASING;
                    }
                }
                else if (getLastElemnt(recentJointPositions).getAxisPosition() <getBeforeLastElemnt(recentJointPositions).getAxisPosition())
                {
                    if (getLastElemnt(recentJointPositions).isAxisMoving())
                    {
                        return MovimentStatus.INCREASING;
                 
                    }
                }
            }
            return MovimentStatus.STOPPED;
        }
        public MovimentStatus getMovimentStatus(int frames)
        {
            if (recentJointPositions.Count > 1)
            {
                if (getLastElemnt(recentJointPositions).getAxisPosition() > getBeforeLastElemnt(recentJointPositions).getAxisPosition())
                {
                    if (getLastElemnt(recentJointPositions).isAxisMoving(frames))
                    {
                        return MovimentStatus.DECREASING;
                    }
                }
                else if (getLastElemnt(recentJointPositions).getAxisPosition() < getBeforeLastElemnt(recentJointPositions).getAxisPosition())
                {
                    if (getLastElemnt(recentJointPositions).isAxisMoving())
                    {
                        return MovimentStatus.INCREASING;

                    }
                }
            }
            return MovimentStatus.STOPPED;
        }
    }
}
