﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjetoKinect.enums;

namespace ProjetoKinect.Model
{
    class JointAxis
    {
        private float position; 
        private int quantity = 1;
        private Axis axis;
        public JointAxis(float position, Axis axis)
        {
            this.position = (float)Math.Round(position, 2);
            this.axis = axis;
        }
        public Axis getAxis() {
            return axis;
        }
        public float getAxisPosition()
        {
            return position;
        }

        public int getAxisQuantity()
        {
            return quantity;
        }
        public void setAxisQuantity(int quantity)
        {
            this.quantity = quantity;
        }
        public void incrementQuantity()
        {
           quantity = quantity+1;         
        }

        internal bool isAxisMoving()
        {
            if (quantity > 4)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        internal bool isAxisMoving(int frames)
        {
            if (quantity > frames)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        internal bool isAxisBodyJointMoving()
        {
            if (quantity < 10)
            {
                return true;
            }
            else { 
                return false;
            }
        }

        public override String ToString()
        {
            return "pos: " + position + " qtd: " + quantity ;
        }
    }
}
