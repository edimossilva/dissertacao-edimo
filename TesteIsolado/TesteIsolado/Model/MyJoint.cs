﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoKinect.enums;

namespace ProjetoKinect.Model
{
    class MyJoint
    {
        private JointMoviment jointMovimentX = new JointMoviment();
        private JointMoviment jointMovimentY = new JointMoviment();
        private JointMoviment jointMovimentZ = new JointMoviment();
        private JointType jointType;

        public MyJoint(Joint joint) {
            setJointType(joint.JointType);
            addPosition(joint.Position);
        }

        private void setJointType(JointType jointType)
        {
            this.jointType = jointType;
        }

        public JointType getJointType() {
            return jointType;
        }

        internal void addPosition(SkeletonPoint skeletonPoint)
        {
            jointMovimentX.addJointPosition(skeletonPoint.X,Axis.AXIS_X);
            jointMovimentY.addJointPosition(skeletonPoint.Y,Axis.AXIS_Y);
            jointMovimentZ.addJointPosition(skeletonPoint.Z,Axis.AXIS_Z);
        }

        internal float getLastX()
        {
            return jointMovimentX.getLastElemnt().getAxisPosition();
        }
        internal float getLastY()
        {
            return jointMovimentY.getLastElemnt().getAxisPosition();
        }
        internal float getLastZ()
        {
            return jointMovimentZ.getLastElemnt().getAxisPosition();
        }

        internal JointMoviment getJointMovimentByAxis(Axis axis)
        {
            if(axis.Equals(Axis.AXIS_X)){
                return jointMovimentX;
            }else if(axis.Equals(Axis.AXIS_Y)){
                return jointMovimentY;
            }else if(axis.Equals(Axis.AXIS_Z)){
                return jointMovimentZ;
            }else{
                return null;
            }
        }
    }
}
