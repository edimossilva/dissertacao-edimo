﻿using ProjetoKinect.Model3d;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ProjetoKinect.Util
{
    class ConverterUtil
    {
        public static MyPoint3D colorToOpenGLMyPoint3D(Color color){
            return new MyPoint3D(color.R / 255, color.G / 255, color.B / 255);
        }

    }
}
