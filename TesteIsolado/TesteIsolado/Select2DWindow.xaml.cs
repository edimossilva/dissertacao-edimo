﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using ProjetoKinect.Model;
using System.Drawing;
using Leap;
using LeapMotionMouse;
using ProjetoKinect.View2d;
using ProjetoKinect.LeapMotionGestures;
namespace ProjetoKinect
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Select2DWindow : Window
    {
        public static readonly DependencyProperty PageLeftEnabledProperty = DependencyProperty.Register(
            "PageLeftEnabled", typeof(bool), typeof(Select2DWindow), new PropertyMetadata(false));

        public static readonly DependencyProperty PageRightEnabledProperty = DependencyProperty.Register(
            "PageRightEnabled", typeof(bool), typeof(Select2DWindow), new PropertyMetadata(false));

        private const int PixelScrollByAmount = 20;
        private const double ScrollErrorMargin = 0.001;
        private KinectController kinectController;
        private String exitButtonTag;
        private Controller lmController = LeapMotionController.getInstance();
        private MouseListener mouseListener;
        public Select2DWindow()
        {
            inicializeAll();
        }

       /* public Select2DWindow(String buttonTag)
        {
            inicializeAll();
            this.gridRegion.Children.Remove(kinectRegion);         
            this.gridRegion.Children.Add(new View2D(buttonTag, this));
            controller.RemoveListener(mouseListener);
            controller.Dispose();
        }*/

        public void inicializeAll()
        {
            InitializeComponent();
            kinectController = new KinectController(this.kinectRegion);
            kinectController.setKinectSensor();
            updateKinectScrollViewerWrapPanel();
            updatePagingButtons();
            initializeLeapMotionMouse();
            
        }
        public void initializeLeapMotionMouse() {
            mouseListener = MouseListener.getInstance();
            lmController = LeapMotionController.getInstance();
           // lmController.AddListener(mouseListener);
            
            mouseListener.setLastClick(MouseListener.ONE_SECOND * 3);
        }
        public void updateKinectScrollViewerWrapPanel()
        {
            this.myWrapPanel.Children.Clear();
            string[] imageArray = Directory.GetFiles(@"C:\Imagens\imagens2D", "*.jpg");
            BitmapImage bi;
            KinectTileButton button;
            for (var index = 0; index < imageArray.Length; index++)
            {
                bi = new BitmapImage(new Uri(imageArray[index]));

                button = new KinectTileButton();
                button.Background = new ImageBrush(bi);
                this.myWrapPanel.Children.Add(button);
                button.Tag = imageArray[index];
            }
            bi = new BitmapImage(new Uri(@"C:\Imagens\Default\sair.png"));
            button = new KinectTileButton();
            button.Background = new ImageBrush(bi);
            this.myWrapPanel.Children.Add(button);
            exitButtonTag = imageArray.Length + "";
            button.Tag = exitButtonTag;
        }

        private void updatePagingButtons()
        {
            this.UpdatePagingButtonState();
            scrollViewer.ScrollChanged += (o, e) => this.UpdatePagingButtonState();
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // Console.Beep();
            //this.kinectController.stopFullSensor();
        }

        private void Close(object sender, System.ComponentModel.CancelEventArgs e)
        {

            this.kinectController.stopFullSensor();
        }

        private void KinectTileButtonClick(object sender, RoutedEventArgs e)
        {
            var button = (KinectTileButton)e.OriginalSource;

            String buttonTag = button.Tag.ToString();
            if (buttonTag.Equals(exitButtonTag))
            {
                this.kinectController.stopFullSensor();
                new ActivitySelector(true).Show();
                this.Close();
            }
            else
            {
               // new Select2DWindow(buttonTag).Show();
                this.gridRegion.Children.Remove(kinectRegion);
                this.gridRegion.Children.Add(new View2D(buttonTag, this));
                lmController.RemoveListener(mouseListener);
         //       this.Close();

            }
            e.Handled = true;

        }

        private void PageLeftButtonClick(object sender, RoutedEventArgs e)
        {
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - PixelScrollByAmount);
        }

        private void PageRightButtonClick(object sender, RoutedEventArgs e)
        {
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset + PixelScrollByAmount);
        }

        private void UpdatePagingButtonState()
        {
            this.PageLeftEnabled = scrollViewer.HorizontalOffset > ScrollErrorMargin;
            this.PageRightEnabled = scrollViewer.HorizontalOffset < scrollViewer.ScrollableWidth - ScrollErrorMargin;

        }

        public bool PageRightEnabled
        {
            get
            {
                return (bool)GetValue(PageRightEnabledProperty);
            }

            set
            {
                this.SetValue(PageRightEnabledProperty, value);
            }
        }

        public bool PageLeftEnabled
        {
            get
            {
                return (bool)GetValue(PageLeftEnabledProperty);
            }

            set
            {
                this.SetValue(PageLeftEnabledProperty, value);
            }
        }

        internal void addKinectRegion()
        {
            this.gridRegion.Children.Add(kinectRegion);
            initializeLeapMotionMouse();
            lmController.AddListener(mouseListener);
           /* kinectController = new KinectController(this.kinectRegion);
            kinectController.setKinectSensor();
            updateKinectScrollViewerWrapPanel();
            updatePagingButtons();

            lmController.AddListener(mouseListener);
            mouseListener.setLastClick(MouseListener.ONE_SECOND * 3);
        */}
    }
}
