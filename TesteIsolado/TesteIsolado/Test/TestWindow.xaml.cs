﻿using Microsoft.Kinect;
using ProjetoKinect.enums;
using ProjetoKinect.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetoKinect.Test
{
    /// <summary>
    /// Interaction logic for TestWindow.xaml
    /// </summary>
    public partial class TestWindow : Window
    {
        private KinectController kinectController;
        private BodyControl bodyControl = new BodyControl();


        public TestWindow()
        {
            InitializeComponent();
            kinectController = new KinectController();
            kinectController.findKinectSensor();
            WindowState = WindowState.Maximized;
            kinectController.enableSkeletonStream(this.SensorSkeletonFrameReady);
        }

        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            Skeleton[] skeletons = kinectController.getSkeletons(e);
               if (skeletons.Length != 0)
               {
                   foreach (Skeleton skel in skeletons)
                   {
                       if (skel.TrackingState == SkeletonTrackingState.Tracked)
                       {
                           bodyControl.setSkeletonJoints(skel);

                           MyJoint leftHandMyJoint = bodyControl.getMyJointByJointType(JointType.HandLeft);
                           MyJoint rightHandMyJoint = bodyControl.getMyJointByJointType(JointType.HandRight);
                           MyJoint leftHand = bodyControl.getMyJointByJointType(JointType.HandLeft);
                           MyJoint rightHand = bodyControl.getMyJointByJointType(JointType.HandRight);
                           MyJoint leftShoulder = bodyControl.getMyJointByJointType(JointType.ShoulderLeft);
                           MyJoint rightShoulder = bodyControl.getMyJointByJointType(JointType.ShoulderRight);
                           MyJoint leftElbow = bodyControl.getMyJointByJointType(JointType.ElbowLeft);
                           MyJoint rightElbow = bodyControl.getMyJointByJointType(JointType.ElbowRight);

                           float leftHandX = leftHand.getJointMovimentByAxis(Axis.AXIS_X).getLastElementPosition();
                           float leftHandY = leftHand.getJointMovimentByAxis(Axis.AXIS_Y).getLastElementPosition();
                           float rightHandX = rightHand.getJointMovimentByAxis(Axis.AXIS_X).getLastElementPosition();
                           float rightHandY = rightHand.getJointMovimentByAxis(Axis.AXIS_Y).getLastElementPosition();


                           float leftShoulderY = leftShoulder.getJointMovimentByAxis(Axis.AXIS_Y).getLastElementPosition();
                           float leftShoulderX = leftShoulder.getJointMovimentByAxis(Axis.AXIS_X).getLastElementPosition();
                           float leftShoulderXAbsolut = leftShoulder.getJointMovimentByAxis(Axis.AXIS_X).getAbsolutValue();

                           float rightShoulderY = rightShoulder.getJointMovimentByAxis(Axis.AXIS_Y).getLastElementPosition();
                           float rightShoulderX = rightShoulder.getJointMovimentByAxis(Axis.AXIS_X).getLastElementPosition();
                           float rightShoulderXAbsolut = rightShoulder.getJointMovimentByAxis(Axis.AXIS_X).getAbsolutValue();

                           float leftElbowY = leftElbow.getJointMovimentByAxis(Axis.AXIS_Y).getLastElementPosition();
                           float leftElbowX = leftElbow.getJointMovimentByAxis(Axis.AXIS_X).getLastElementPosition();
                           float leftElbowXAbsolut = leftElbow.getJointMovimentByAxis(Axis.AXIS_X).getAbsolutValue();

                           float rightElbowY = rightElbow.getJointMovimentByAxis(Axis.AXIS_Y).getLastElementPosition();
                           float rightElbowX = rightElbow.getJointMovimentByAxis(Axis.AXIS_X).getLastElementPosition();
                           float rightElbowXAbsolut = rightElbow.getJointMovimentByAxis(Axis.AXIS_X).getAbsolutValue();

                           text1.Text = "Left Y => S = " + leftShoulderY + " // E = " + leftElbowY + " // H = " + leftHandY;
                           text2.Text = "Left X => S = " + leftShoulderX + " // E = " + leftElbowX + " // H = " + leftHandX;

                           text3.Text = "Right Y => S = " + rightShoulderY + " // E = " + rightElbowY + " // H = " + rightHandY;
                           text4.Text = "Right X => S = " + rightShoulderX + " // E = " + rightElbowX + " // H = " + rightHandX;

                           text5.Text = bodyControl.isTwoHandsFrontPosition(null) + "";

                       }
                   }
               


            }
        }
    }
}
