﻿using Leap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloLeapMotion
{
    class LeapMotionGestureHands : LeapMotionGesture
    {
        public Boolean areTwoHandsOn()
        {
            if (frame.Hands.Count == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean isOneHandOn()
        {
            if (frame.Hands.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void showInfo()
        {
            Console.WriteLine("areTwoHands: " + areTwoHandsOn());
        }

        public int getFingersCount()
        {
            HandList handList = frame.Hands;
            FingerList fingerList = frame.Fingers;
            int maxFingers = 0;

            foreach (Hand hand in handList)
            {
                if (hand.Fingers.Count > maxFingers)
                {
                    maxFingers = hand.Fingers.Count;
                }
            }

            return maxFingers;
        }

        internal bool isOneHandClosed()
        {
            if (isOneHandOn())
            {
                if (getFingersCount() == 0)
                {
                    return true;
                }
            }

            return false;

        }

        internal int getHandLeftFingersCount()
        {
            Hand hand = frame.Hands.Leftmost;
            return hand.Fingers.Count;
        }

        internal int getHandRightFingersCount()
        {
            Hand hand = frame.Hands.Rightmost;
            return hand.Fingers.Count;
        }

        internal bool areTwoHandsFingerCount(int p)
        {
            if (getHandLeftFingersCount() == p &&
                        getHandRightFingersCount() == p)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal bool hasAnyHandClosed()
        {
            if (getHandLeftFingersCount() == 0)
            {
                return true;
            }
            else if (getHandRightFingersCount() == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        internal bool hasAnyHandOpen()
        {
            if (getHandLeftFingersCount() == 5)
            {
                return true;
            }
            else if (getHandRightFingersCount() == 5)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
