﻿using Leap;
using ProjetoKinect.enums;
using ProjetoKinect.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloLeapMotion
{
    class LeapMotionGestureHandOscilation : LeapMotionGesture
    {

        private JointMoviment jointMovment = new JointMoviment();
       
        public void setFrame(Frame frame, Axis axis)
        {
            this.frame = frame;
            int positionInt;
            if (frame.Hands.Count == 1)
            {
                if (axis == Axis.AXIS_X)
                {
                    positionInt = normalizePosition(frame.Hands[0].PalmPosition.x);
                    jointMovment.addJointPosition(positionInt, Axis.AXIS_X);
                }
                else if (axis == Axis.AXIS_Y)
                {
                    positionInt = normalizePosition(frame.Hands[0].PalmPosition.y);
                    jointMovment.addJointPosition(positionInt, Axis.AXIS_Y);
                }
                else if (axis == Axis.AXIS_Z)
                {
                    positionInt = normalizePosition(frame.Hands[0].PalmPosition.z);
                    jointMovment.addJointPosition(positionInt, Axis.AXIS_Z);
                }

            }
        }

        protected int normalizePosition(float value)
        {
            int positionInt = (int)value;
            positionInt = (positionInt / 10) / 2;
            return positionInt;
        }

        public MovimentStatus getMovmentStatus()
        {
            return jointMovment.getMovimentStatus();
        }




    }
}
