﻿using Leap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloLeapMotion
{
    class LeapMotionGesture
    {

        protected Frame frame { get; set; }

        public void setFrame(Frame frame)
        {
            this.frame = frame;
        }

        public static bool isKeyTap(Gesture gesture)
        {
            if (gesture.Type.Equals(Gesture.GestureType.TYPEKEYTAP))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isCircular()
        {
            return isGesture(Gesture.GestureType.TYPECIRCLE);
        }

        internal bool isTap()
        {
            return isGesture(Gesture.GestureType.TYPEKEYTAP);
        }

        internal bool isSwipe()
        {
            return isGesture(Gesture.GestureType.TYPESWIPE); ;
        }
        
        private bool isGesture(Leap.Gesture.GestureType gestureType)
        {
            GestureList gestures = frame.Gestures();
            for (int i = 0; i < gestures.Count; i++)
            {
                Gesture gesture = gestures[i];

                if (gesture.Type == gestureType)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
