﻿using HelloLeapMotion;
using Leap;
using ProjetoKinect.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoKinect.LeapMotionGestures
{
    class LeapMotionController
    {
        private static Controller controller;
        private LeapMotionGestureHands handsGesture = new LeapMotionGestureHands();
        private LeapMotionGestureKeyTap keyTapGesture = new LeapMotionGestureKeyTap();
        private LeapMotionGestureHandOscilation handXOscilationGesture = new LeapMotionGestureHandOscilation();
        private LeapMotionGestureHandOscilation handYOscilationGesture = new LeapMotionGestureHandOscilation();
        private LeapMotionGestureHandOscilation handZOscilationGesture = new LeapMotionGestureHandOscilation();

        public LeapMotionController()
        {
            controller = getInstance();
            enableGestures();
        }

        public static  Controller getInstance()
        {
            if (controller == null) {
                controller = new Controller();
            }
            return controller;
        }
               
        public void enableGestures() {
            controller.EnableGesture(Gesture.GestureType.TYPECIRCLE);
            controller.EnableGesture(Gesture.GestureType.TYPEKEYTAP);
            controller.EnableGesture(Gesture.GestureType.TYPESCREENTAP);
            controller.EnableGesture(Gesture.GestureType.TYPESWIPE);
        }

        public LeapMotionGestureHands getHandsGesture()
        {
            return handsGesture;
        }
        public LeapMotionGestureKeyTap getkeyTapGesture()
        {
            return keyTapGesture;
        }
        public LeapMotionGestureHandOscilation getHandXOscilationGesture()
        {
            return handXOscilationGesture;
        }
        public LeapMotionGestureHandOscilation getHandYOscilationGesture()
        {
            return handYOscilationGesture;
        }
        public LeapMotionGestureHandOscilation getHandZOscilationGesture()
        {
            return handZOscilationGesture;
        }
        public Frame getFrame()
        {
            return controller.Frame();
        }

        internal void update()
        {
          //  Frame frame = getFrame();
            update(getFrame());
        }
        public void update(Frame frame)
        {
            handsGesture.setFrame(frame);
            keyTapGesture.setFrame(frame);
            handXOscilationGesture.setFrame(frame, Axis.AXIS_X);
            handYOscilationGesture.setFrame(frame, Axis.AXIS_Y);
            handZOscilationGesture.setFrame(frame, Axis.AXIS_Z);
            
        }
    }
}
