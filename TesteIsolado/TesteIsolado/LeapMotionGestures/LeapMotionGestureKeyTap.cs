﻿using Leap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloLeapMotion
{
    class LeapMotionGestureKeyTap 
    {

        private Vector normalizedDirection { get; set; }
        private KeyTapGesture keytap { get; set; }
        private Direction lastDirection { get; set; }
        private bool isGestureRecognized { get; set; }
        private long currentTime, previousTime, timeChange;

        public void addMovment(Gesture gesture)
        {
            if (gesture == null)
            {
                isGestureRecognized = false;
            }
            else
            {
                keytap = new KeyTapGesture(gesture);
                normalizedDirection = normalizeDirectionVector(keytap.Direction);
                if (lastDirection == getDirection())
                {
                    this.currentTime = DateTime.Now.Millisecond;
                    isGestureRecognized = true;
                }
                else
                {
                    isGestureRecognized = false;
                }
            }
        }

        public bool hasTap()
        {
            this.previousTime = currentTime;
            this.timeChange = DateTime.Now.Millisecond - previousTime;
            if (isGestureRecognized && timeChange > 100)
            {
                isGestureRecognized = false;
                return true;
            }
            else
            {
                return false;
            }

        }

        public Direction getDirection()
        {
            if (normalizedDirection != null)
            {
                if (normalizedDirection.y <= -0.8)
                {
                    if (normalizedDirection.x <= 0.2 || normalizedDirection.x >= -0.2)
                    {
                        if (normalizedDirection.y <= 0.2 || normalizedDirection.y >= -0.2)
                        {
                            return Direction.DOWN;
                        }
                    }
                }
            }
            return Direction.UNCKNOW;
        }

        private Vector normalizeDirectionVector(Vector vector)
        {
            Vector normalizedVector = new Vector();
            normalizedVector.x = (float)Math.Round(vector.x, 1);
            normalizedVector.y = (float)Math.Round(vector.y, 1);
            normalizedVector.z = (float)Math.Round(vector.z, 1);
            return normalizedVector;
        }

        public void showInfo()
        {
            Console.WriteLine("Direction: " + normalizedDirection + "DirectionDown: " + getDirection());
        }

        internal void setFrame(Frame frame)
        {
            GestureList gestures = frame.Gestures();
            for (int i = 0; i < gestures.Count; i++)
            {
                Gesture gesture = gestures[i];
                if (LeapMotionGesture.isKeyTap(gesture))
                {
                    addMovment(gesture);
                }
            }
        }

        internal void clear()
        {
            normalizedDirection = null;
          //  currentTime = DateTime.Now.Millisecond;
        }
    }
}
